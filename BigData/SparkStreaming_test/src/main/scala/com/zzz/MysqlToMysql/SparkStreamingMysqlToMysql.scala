package com.zzz.MysqlToMysql

import java.util.Properties

import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.log4j.{Level, LogManager, Logger}
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}
import org.apache.spark.streaming.dstream.InputDStream
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe
import org.apache.spark.streaming.kafka010.{CanCommitOffsets, HasOffsetRanges, KafkaUtils, LocationStrategies}
import org.apache.spark.streaming.{Seconds, StreamingContext}



object SparkStreamingMysqlToMysql {
  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.ERROR)
    /** ----- init args value start ----- **/
    if (args.length < 1) {
      System.err.println("Usage: args参数不正确 [1]-config properties")
      System.exit(1)
    }

    val configFile = args(0)
    /** ----- init args value stop ----- **/

    val Array(kafkaBrokers,kafkaTopics, kuduMaster, kuduTableName, seconds, debugFlag, groupId,mysqlTableName,filterSql,selectExpr,iudSql,autooffsetreset) = parseArgsFromFile(configFile)

//    println(kafkaBrokers,kafkaTopics, kuduMaster, kuduTableName, seconds, debugFlag, groupId,mysqlTableName,filterSql,selectExpr,iudSql,autooffsetreset)

    val ssc = createContext(kafkaBrokers, kafkaTopics, kuduMaster, kuduTableName, seconds.toInt, groupId, debugFlag.toBoolean, mysqlTableName,filterSql,selectExpr,iudSql,autooffsetreset)
    ssc.start()
    LogManager.getRootLogger.setLevel(Level.WARN)
    ssc.awaitTermination()
  }


  //计算主体
  def createContext(kafkaBrokers: String, kafkaTopics: String, kuduMaster: String, kuduTableName: String, interval: Int, groupId: String, debugFlag: Boolean, mysqlTableName: String,filterSql:String,selectExpr:String,iudSql:String,autooffsetreset:String) = {
    val ssc = initSparkStreamingContext(interval)
    //val bc_debug = ssc.sparkContext.broadcast(debugFlag)
    //多个topic使用,分隔

    val kafkaParams = Map[String, Object](
      "bootstrap.servers" -> kafkaBrokers,
      "key.deserializer" -> classOf[StringDeserializer],
      "value.deserializer" -> classOf[StringDeserializer],
      "group.id" -> groupId,
      "auto.offset.reset" -> autooffsetreset,
      "enable.auto.commit" -> (false: java.lang.Boolean)
    )


    val topics = Array(kafkaTopics)
    val dStream: InputDStream[ConsumerRecord[String, String]] = KafkaUtils.createDirectStream[String, String](
      ssc,
      LocationStrategies.PreferConsistent,
      Subscribe[String, String](topics, kafkaParams)
    )

    dStream.foreachRDD(rdd => {

      if (rdd != null){
//      if (!rdd.isEmpty()) {
        val offsetRanges = rdd.asInstanceOf[HasOffsetRanges].offsetRanges
        val spark: SparkSession = SparkSessionSingleton.getInstance(rdd.sparkContext.getConf)

        import spark.implicits._

        val jsonRDD: RDD[String] = rdd.map(record => record.value())
        val jsonDS: Dataset[String] = jsonRDD.toDS()
        val jsonDF: DataFrame = spark.read.json(jsonDS)
        jsonDF.show()

        jsonDF.createOrReplaceTempView("allResult")
//        val filterDF: DataFrame = spark.sql("select * from allResult ")
        val filterDF: DataFrame = spark.sql("select data.id,data.name,data.age from allResult where data IS NOT NULL AND table ='people' AND database ='kkbtest' ")
        filterDF.show()


//
//        val insertDF: DataFrame = spark.sql(iudSql + " WHERE type = 'INSERT' ")
//        val updateDF: DataFrame = spark.sql(iudSql + " WHERE type = 'UPDATE' ")
//        val deleteDF: DataFrame = spark.sql(iudSql + " WHERE type = 'DELETE' ")

        // if(!insertDF.isEmpty && insertDF!=null) {
        // kuduContext.upsertRows(insertDF,kuduTableName)
        // }

        // if(!updateDF.isEmpty && updateDF!=null) {
        //   kuduContext.upsertRows(updateDF,kuduTableName)
        // }

        // if(!deleteDF.isEmpty && deleteDF!=null) {
        //   kuduContext.deleteRows(deleteDF,kuduTableName)
        // }


        dStream.asInstanceOf[CanCommitOffsets].commitAsync(offsetRanges)

      }
    })

    ssc
  }


  /** initSparkStreamingContext **/
  def initSparkStreamingContext(interval: Int) = {
    val conf = new SparkConf()
      .setAppName("MysqlKafkaKudu-SparkStreaming")
      .setMaster("local[*]")
    //启用反压机制
    conf.set("spark.streaming.backpressure.enabled","true")
    //最小摄入条数控制
    conf.set("spark.streaming.backpressure.pid.minRate","1")
    //最大摄入条数控制
    conf.set("spark.streaming.kafka.maxRatePerPartition","500")
    //初始最大接收速率控制
    conf.set("spark.streaming.backpressure.initialRate","100")
    //
    conf.set("spark.streaming.kafka.consumer.poll.ms", "3000")
    conf.set("spark.task.maxFailures","10")
    //优雅停机
    conf.set("spark.streaming.stopGracefullyOnShutdown","true")

    val sc = new SparkContext(conf)
    val ssc = new StreamingContext(sc, Seconds(interval))
    ssc
  }

  /** Lazily instantiated singleton instance of SparkSession */
  object SparkSessionSingleton {
    @transient private var instance: SparkSession = _

    def getInstance(sparkConf: SparkConf): SparkSession = {
      if (instance == null) {
        instance = SparkSession
          .builder
          .config(sparkConf)
          .getOrCreate()
      }
      instance
    }
  }

  def parseArgsFromFile(fileName: String): Array[String] = {
    val properties: Properties = new Properties
    import java.io.{BufferedInputStream, FileInputStream}
    val in = new BufferedInputStream(new FileInputStream(fileName))
    properties.load(in)
    //需要获取的参数
    val paramNames = Array("kafkaBrokers","kafkaTopics", "kuduMaster", "kuduTableName", "seconds", "debugFlag", "groupId","mysqlTableName","filterSql","selectExpr","iudSql","autooffsetreset")
    val paramValues = paramNames.map { x =>
      properties.getProperty(x)
    }
    paramValues
  }
}
