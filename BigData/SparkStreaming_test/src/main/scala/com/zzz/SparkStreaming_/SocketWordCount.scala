package com.zzz.SparkStreaming_



import org.apache.log4j.{Level, Logger}
import org.apache.spark.SparkConf
import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.dstream.{DStream, ReceiverInputDStream}
import org.apache.spark.streaming.{Seconds, StreamingContext}


object SocketWordCount {

  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.ERROR)
    val sparkConf: SparkConf = new SparkConf().setAppName("TcpWordCount").setMaster("local[*]")
    val ssc: StreamingContext = new StreamingContext(sparkConf, Seconds(3))

    val sockettextStream: ReceiverInputDStream[String] = ssc.socketTextStream("localhost", 9999)
    val result: DStream[(String, Int)] = sockettextStream.flatMap(_.split(" ")).map((_, 1)).reduceByKey(_ + _)
    result.print()

    ssc.start()
    ssc.awaitTermination()
  }
}
