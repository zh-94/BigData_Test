package com.zzz.mr;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class MyMapper extends Mapper<LongWritable, Text,Text, IntWritable> {
    /**
     *
     * @param key   : 偏移量
     * @param value : 当前行数据  Dear,Bear,River
     * @param context
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        // 获取当前行数据  Text类型转string类型
        String line = value.toString();

        // 切分成一个个单词
        String[] words = line.split(",");

        // 将每个单词转换成（word,1）的形式，输出出去
        for (String word : words){
            Text keyout = new Text(word);
            IntWritable valueout = new IntWritable(1);
            context.write(keyout,valueout);
        }
    }
}
