package com.zzz.combinetextinputformat;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * (Dear,1)
 * (hive,1)
 * (Dear,1)
 * (Dear,1)
 * (hive,1)
 *
 * ->分组
 * (hive,1)
 * (hive,1)
 *  =》hive list(1,1,1,1,)
 *
 * (Dear,1)
 * (Dear,1)
 * (Dear,1)
 */
public class MyReducer extends Reducer<Text, IntWritable,Text, IntWritable> {
    /**
     *
     * @param key  hive
     * @param values   list(1,1,1,1,)
     * @param context
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    protected void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
        int sum = 0;
        for(IntWritable num : values){
            int count = num.get();   //IntWritable 转为 int
            sum += count ;
        }
        //(word,totalCount)
        context.write(key,new IntWritable(sum));
    }
}
