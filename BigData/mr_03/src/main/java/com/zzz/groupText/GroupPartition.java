package com.zzz.groupText;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Partitioner;

public class GroupPartition extends Partitioner<OrderBean, NullWritable> {
    int mypartition = 0;
    @Override
    public int getPartition(OrderBean orderBean, NullWritable nullWritable, int numReduceTasks) {
        mypartition = (orderBean.getOrderId().hashCode() & Integer.MAX_VALUE) % numReduceTasks;
        return mypartition;
    }
}
