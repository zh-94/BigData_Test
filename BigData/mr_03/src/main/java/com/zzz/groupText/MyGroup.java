package com.zzz.groupText;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class MyGroup extends WritableComparator {
    public MyGroup() {
        super(OrderBean.class,true);
    }

    @Override
    public int compare(WritableComparable a, WritableComparable b) {
        OrderBean first = (OrderBean) a;
        OrderBean second = (OrderBean) b;

        //以orderId作为比较条件，判断哪些orderid相同作为同一组
        return first.getOrderId().compareTo(second.getOrderId()); //0为一组，1为不同一组
    }
}
