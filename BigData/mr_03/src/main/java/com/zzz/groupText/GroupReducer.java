package com.zzz.groupText;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

//求每个组当中的top2的订单金额数据
public class GroupReducer extends Reducer<OrderBean, NullWritable,OrderBean,NullWritable> {
    @Override
    protected void reduce(OrderBean key, Iterable<NullWritable> values, Context context) throws IOException, InterruptedException {
        int i = 0;
        for (NullWritable value : values){
            System.out.println("value是： " + value);
            if (i < 2){
                context.write(key,NullWritable.get());
                i++;
            }
        }
    }
}
