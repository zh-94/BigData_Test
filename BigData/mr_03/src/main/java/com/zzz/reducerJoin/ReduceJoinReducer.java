package com.zzz.reducerJoin;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class ReduceJoinReducer extends Reducer<Text,Text,Text, NullWritable> {
    /**
     * key  list(value1,value2)
     * @param key
     * @param values
     * @param context
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        String firstPart = "";
        String secondPart = "";

        for (Text value : values){
            if (value.toString().startsWith("p")){
                firstPart = value.toString();
            }else {
                secondPart = value.toString();
            }
        }
        context.write(new Text(firstPart + "\t" + secondPart),NullWritable.get());
    }
}
